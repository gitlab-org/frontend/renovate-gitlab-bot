export const RENOVATE_PROJECT_ID = "15445883";
export const RENOVATE_PROJECT_PATH = "gitlab-org/frontend/renovate-gitlab-bot";
export const DRY_RUN = (process.env.DRY_RUN ?? "true") === "true";
export const RENOVATE_BOT_USER = "gitlab-dependency-update-bot";
export const RENOVATE_STOP_UPDATING_LABEL = "automation:bot-no-updates";
export const RENOVATE_WEB_URL = `https://gitlab.com/${RENOVATE_PROJECT_PATH}`;

const {
  createServerConfig,
  baseConfig,
  defaultLabels,
  availableRouletteReviewerByRole,
} = require("../lib/shared");
const { updateDangerReviewComponent } = require("../lib/components");

module.exports = createServerConfig([
  {
    repository: "gitlab-renovate-forks/duo-workflow-service",
    ...baseConfig,
    labels: [...defaultLabels, "Category:Duo Workflow"],
    reviewers: availableRouletteReviewerByRole("duo-workflow-service"),
    enabledManagers: ["custom.regex"],
    semanticCommits: "enabled",
    reviewersSampleSize: 1,
    prConcurrentLimit: 4,
    semanticCommitType: "chore",
    ...updateDangerReviewComponent,
  },
]);

#!/usr/bin/env node

import { DateTime } from "luxon";

import { log, setScope, warn } from "../bot_image/lib/logger.mjs";
import {
  RENOVATE_PROJECT_ID,
  RENOVATE_PROJECT_PATH,
} from "../bot_image/lib/constants.mjs";
import { GitLabAPI } from "../bot_image/lib/api.mjs";
import type { AxiosResponse } from "axios";

setScope(`[Deleting pipelines]`);

const MAX_RUNTIME_MINUTES = 10;

const STOP_SCRIPT_AT = DateTime.now().plus({ minutes: MAX_RUNTIME_MINUTES });
const FOUR_WEEKS_AGO = DateTime.now().minus({ days: 28 });

async function deletePipelines(pipelineURLs: string[]) {
  await Promise.all(pipelineURLs.map((url) => GitLabAPI.delete(url)));
}

const query = `
query oldScheduledPipelines($project: ID!, $updatedBefore: Time, $ref: String) {
  project(fullPath: $project) {
    pipelines(
      source: "schedule"
      ref: $ref
      updatedBefore: $updatedBefore
      last: 20
    ) {
      pageInfo {
        hasPreviousPage
      }
      nodes {
        ...pipeline
        downstream {
          nodes {
            ...pipeline
          }
        }
      }
    }
  }
}

fragment pipeline on Pipeline {
  id
  createdAt
  child
  path
}
`;

type GraphQLError = {
  message: string;
};

type GraphQLResponse<T> = {
  data: T;
  errors: GraphQLError[] | null;
};

type Pipeline = {
  id: `gid://gitlab/Ci::Pipeline/${number}`;
  createdAt: string;
  child: boolean;
  path: string;
  downstream: { nodes: Pipeline[] } | null;
};

type PipelineResponse = {
  project: {
    pipelines: {
      pageInfo: {
        hasNextPage: boolean;
      };
      nodes: Pipeline[];
    };
  };
};

async function graphql<T>(
  query: string,
  variables: any
): Promise<AxiosResponse<GraphQLResponse<T>>> {
  return GitLabAPI.post("https://gitlab.com/api/graphql", { query, variables });
}

function getIdFromGid(s: string): string {
  return s.substring(s.lastIndexOf("/") + 1);
}

async function main() {
  let c = 0;
  let stack: string[] = [];
  let hasNextPage = true;
  while (hasNextPage) {
    const { data } = await graphql<PipelineResponse>(query, {
      ref: "main",
      updatedBefore: FOUR_WEEKS_AGO,
      project: RENOVATE_PROJECT_PATH,
    });
    if (data?.errors?.length) {
      warn(`GraphQL Query returned some errors`, data.errors);
      break;
    }
    hasNextPage = data.data.project.pipelines.pageInfo.hasNextPage;
    const pipelines = data.data.project.pipelines.nodes;
    for (const pipeline of pipelines) {
      c += 1;
      const { createdAt, path, downstream } = pipeline;
      const id = getIdFromGid(pipeline.id);

      setScope(path);

      if (FOUR_WEEKS_AGO <= DateTime.fromISO(createdAt)) {
        log(`Pipeline too new. Skipping.`);
        continue;
      }

      log(`Pipeline is older than four weeks, deleting`);
      for (const child of downstream?.nodes ?? []) {
        if (child.child) {
          log(`Child Pipeline ${child.path} will be deleted.`);
          stack.push(
            `/projects/${RENOVATE_PROJECT_ID}/pipelines/${getIdFromGid(
              child.id
            )}`
          );
        }
      }

      stack.push(`/projects/${RENOVATE_PROJECT_ID}/pipelines/${id}`);
    }
    setScope("Deleting...");
    while (stack.length > 0) {
      const parallel = stack.splice(0, 10);
      await deletePipelines(parallel);
      log(`Deleted ${parallel.length} pipelines`);
    }

    setScope("");

    if (DateTime.now() > STOP_SCRIPT_AT) {
      log(`Script ran for more than ${MAX_RUNTIME_MINUTES} minutes, stopping`);
      break;
    }
  }

  log(`Processed ${c} pipelines`);
}

main()
  .then(() => {
    log("Done");
  })
  .catch((e) => {
    warn("An error happened");
    warn(e.message);
    warn(e.stack);
    process.exitCode = 1;
  });

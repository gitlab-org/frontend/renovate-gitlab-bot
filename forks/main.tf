terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "17.7.0"
    }
  }

  backend "http" {}
}

provider "gitlab" {
  token = var.gitlab_renovate_bot_token
}

provider "gitlab" {
  token = var.gitlab_bot_token

  alias = "bot"
}

variable "gitlab_renovate_bot_token" {
  description = "GitLab Token for the gitlab-renovate-forks group bot"
  type        = string
  sensitive   = true
}

variable "gitlab_bot_token" {
  description = "GitLab Token for the gitlab bot used for pull mirroring in forks of private upstream projects"
  type        = string
  sensitive   = true
}

variable "projects" {
  description = "A list of projects to manage with renovate. The projects must be specified with their path with namespace, e.g. `gitlab-org/gitlab`."
  type = list(object({
    path       = string
    fork_path  = optional(string)
    is_private = optional(bool)
  }))
}

data "gitlab_current_user" "this" {}

// renaming this data source from "upstream" to "upstream_projects" because of (a potential) OpenTofu bug:
// https://github.com/opentofu/opentofu/issues/2355
data "gitlab_project" "upstream_projects" {
  for_each = { for project in var.projects : project.path => project }

  path_with_namespace = each.key

  lifecycle {
    postcondition {
      condition     = (self.visibility_level == "public" && self.repository_access_level == "enabled") || each.value.is_private == true
      error_message = <<-EOT
        Project ${each.key} has visibility_level '${self.visibility_level}' and repository_access_level '${self.repository_access_level}'
        but is not marked as private (is_private = true) in the configuration.
        Non-public projects must be explicitly marked as private to properly setup mirroring in the fork."
      EOT
    }
  }
}

# Verify if bot user has Developer access for the private
# The bot must at least be able to read the project (Guest role) otherwise,
# the upstream data source will 404 NOT FOUND anyways and this data source
# won't be evaluated at all.
data "gitlab_project_membership" "private_project_access" {
  for_each = { for project in var.projects : project.path => project if project.is_private == true }

  inherited  = true
  project_id = data.gitlab_project.upstream_projects[each.key].id
  query      = data.gitlab_current_user.this.username
  # NOTE: should use this once https://gitlab.com/gitlab-org/terraform-provider-gitlab/-/merge_requests/2257 is released
  # user_ids   = [data.gitlab_current_user.this.id]

  lifecycle {
    postcondition {
      condition = length([
        for member in self.members :
        member
        if member.username == data.gitlab_current_user.this.username && (
          member.access_level == "developer" ||
          member.access_level == "maintainer" ||
          member.access_level == "owner"
        )
      ]) > 0
      error_message = <<-EOT
        The renovate bot user (${data.gitlab_current_user.this.username}) is not at least a Developer of the private project ${each.key}.
        Membership is required for private projects.
      EOT
    }
  }
}

data "gitlab_group" "forks" {
  full_path = "gitlab-renovate-forks"
}

resource "gitlab_deploy_token" "private_projects" {
  provider = gitlab.bot
  for_each = { for project in var.projects : project.path => project if project.is_private == true }

  project  = each.key
  name     = "Pull mirror token for the Renovate bot, see https://gitlab.com/gitlab-org/frontend/renovate-gitlab-bot"
  username = "renovate-bot-pull-mirror"
  scopes   = ["read_repository"]
}

resource "gitlab_project" "forks" {
  # Create a forked project for every project configuration
  for_each = { for project in var.projects : project.path => project }

  # Fork from upstream project
  forked_from_project_id = data.gitlab_project.upstream_projects[each.key].id

  # Specify some project name and parent group
  name             = data.gitlab_project.upstream_projects[each.key].name
  path             = coalesce(each.value.fork_path, data.gitlab_project.upstream_projects[each.key].path)
  description      = data.gitlab_project.upstream_projects[each.key].description
  namespace_id     = data.gitlab_group.forks.id
  visibility_level = data.gitlab_project.upstream_projects[each.key].visibility_level

  # Settings from the upstream project that matter for the fork
  ci_config_path = data.gitlab_project.upstream_projects[each.key].ci_config_path

  # Pull Mirroring settings
  import_url                          = data.gitlab_project.upstream_projects[each.key].http_url_to_repo
  import_url_username                 = each.value.is_private == true ? gitlab_deploy_token.private_projects[each.key].username : null
  import_url_password                 = each.value.is_private == true ? gitlab_deploy_token.private_projects[each.key].token : null
  mirror                              = true
  mirror_trigger_builds               = false
  mirror_overwrites_diverged_branches = true
  only_mirror_protected_branches      = true

  lifecycle {
    prevent_destroy = true
  }
}

# For private projects, we should allow that the CI job token of upstream project is allowed to access
# the fork. Otherwise checking out the code in CI jobs might fail
# See also: https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#control-job-token-access-to-your-project
resource "gitlab_project_job_token_scopes" "private_projects_job_token_access" {
  for_each = { for project in var.projects : project.path => project if project.is_private == true }

  project = gitlab_project.forks[each.key].id
  target_project_ids = [
    data.gitlab_project.upstream_projects[each.key].id,
  ]
}

output "renovate_forks" {
  value = [
    for k, v in gitlab_project.forks : { "id" = v.id, "name" = v.name, "path_with_namespace" = v.path_with_namespace, "web_url" : v.web_url }
  ]
}
